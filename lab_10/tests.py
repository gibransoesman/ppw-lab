from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Pengguna, MovieKu
from .views import index
from .utils import get_my_movies_from_database, get_list_movie_from_api
from http.cookies import SimpleCookie

# Create your tests here.
class Lab10UnitTest(TestCase):
	def test_lab_10_is_exist(self):
		response = Client().get('/lab-10/')
		self.assertEqual(response.status_code,200)

	def test_using_index_func(self):
		found = resolve('/lab-10/')
		self.assertEqual(found.func, index)

	def test_login_session_lab_10(self):
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session.save()
		response = self.client.get('/lab-10/')
		self.assertEqual(response.status_code,302)

	def test_dashboard_lab_10_not_login(self):
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 302)

	def test_dashboard_lab_10_already_login_some_perintilan_set(self):
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['access_token'] = 'something'
		session['kode_identitas'] = '1606878505'
		session['role'] = 'mahasiswa'
		session['movies'] = 'dummies'
		session.save()
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/dashboard/') #after pengguna saved
		self.assertEqual(response.status_code, 200)

	def test_movie_list_lab_10_already_login_some_perintilan_not_set(self):
		response = self.client.get('/lab-10/movie/list/')
		self.assertEqual(response.status_code, 302)
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['access_token'] = 'something'
		session['kode_identitas'] = '1606878505'
		session['role'] = 'mahasiswa'
		session.save()
		response = self.client.get('/lab-10/movie/list/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/movie/list/?judul=batman&tahun=2017')
		self.assertEqual(response.status_code, 200)

	def test_movie_details_lab_10_not_login(self):
		response = self.client.get('/lab-10/movie/detail/tt3198652/')
		self.assertEqual(response.status_code, 200)

	def test_movie_details_lab_10_login(self):
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['access_token'] = 'something'
		session['kode_identitas'] = '1606878505'
		session['role'] = 'mahasiswa'
		session.save()
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/movie/detail/tt3198652/')
		self.assertEqual(response.status_code, 200)

	def test_add_watch_later_lab_10_not_login(self):
		response = self.client.get('/lab-10/movie/watch_later/add/tt3198652/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/movie/watch_later/add/tt3198652/')
		self.assertEqual(response.status_code, 302)

	def test_add_watch_later_lab_10_not_login_have_movie_session(self):
		session = self.client.session
		session['movies'] = []
		session.save()
		response = self.client.get('/lab-10/movie/watch_later/add/tt3198652/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/movie/watch_later/add/tt3198652/')
		self.assertEqual(response.status_code, 302)

	def test_add_watch_later_lab_10_login(self):
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['access_token'] = 'something'
		session['kode_identitas'] = '1606878505'
		session['role'] = 'mahasiswa'
		session.save()
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/movie/watch_later/add/tt3198652/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/movie/watch_later/add/tt3198652/')
		self.assertEqual(response.status_code, 302)

	def test_list_watch_later_lab_10_not_login(self):
		response = self.client.get('/lab-10/movie/watch_later/')
		self.assertEqual(response.status_code, 302)

	def test_list_watch_later_lab_10_login(self):
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['access_token'] = 'something'
		session['kode_identitas'] = '1606878505'
		session['role'] = 'mahasiswa'
		session.save()
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/movie/watch_later/')
		self.assertEqual(response.status_code, 200)

	def test_list_watch_later_lab_10_login_more_movies(self):
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['access_token'] = 'something'
		session['kode_identitas'] = '1606878505'
		session['role'] = 'mahasiswa'
		session.save()
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/movie/watch_later/add/tt3198652/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/movie/watch_later/add/tt3198652/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/movie/watch_later/')
		self.assertEqual(response.status_code, 200)

	def test_search_in_omdb_api_lab_10_none(self):
		response = self.client.get('/lab-10/api/movie/-/-/')
		self.assertEqual(response.status_code, 200)
		
	def test_search_in_omdb_api_lab_10_not_none(self):
		response = self.client.get('/lab-10/api/movie/Chris Pratt Shows You Around the Set of Guardians of the Galaxy Vol. 2/-/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/api/movie/batman/2017/')
		self.assertEqual(response.status_code, 200)

	def test_delete_movies_watch_later(self):
		response = self.client.get('/lab-10/movie/watch_later/delete/12ewe12eqwe21/')
		self.assertEqual(response.status_code, 302)
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['access_token'] = 'something'
		session['kode_identitas'] = '1606878505'
		session['role'] = 'mahasiswa'
		session.save()
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/movie/watch_later/delete/12ewe12eqwe21/')
		self.assertEqual(response.status_code, 302)

	def test_add_watched_movies(self):
		response = self.client.get('/lab-10/movie/watched_movie/add/12ewe12eqwe21/')
		self.assertEqual(response.status_code, 302)
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['access_token'] = 'something'
		session['kode_identitas'] = '1606878505'
		session['role'] = 'mahasiswa'
		session.save()
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/movie/watched_movie/add/12ewe12eqwe21/')
		self.assertEqual(response.status_code, 302)

	def test_watched_movies_list(self):
		response = self.client.get('/lab-10/movie/watched_movie/')
		self.assertEqual(response.status_code, 302)
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['access_token'] = 'something'
		session['kode_identitas'] = '1606878505'
		session['role'] = 'mahasiswa'
		session.save()
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/movie/watched_movie/add/12ewe12eqwe21/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/movie/watched_movie/add/12ewe1w2eqwe21/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/movie/watched_movie/')
		self.assertEqual(response.status_code, 200)

	def test_delete_watched_movies(self):
		response = self.client.get('/lab-10/movie/watched_movie/delete/12ewe12eqwe21/')
		self.assertEqual(response.status_code, 302)
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['access_token'] = 'something'
		session['kode_identitas'] = '1606878505'
		session['role'] = 'mahasiswa'
		session.save()
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-10/movie/watched_movie/delete/12ewe12eqwe21/')
		self.assertEqual(response.status_code, 302)

