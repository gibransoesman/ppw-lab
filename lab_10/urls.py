from django.conf.urls import url
from .views import *

from package.package_for_lab_10.custom_auth import auth_login, auth_logout

urlpatterns = [
    # custom auth
    url(r'^custom_auth/login/$', auth_login, name='auth_login'),
    url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),

    # index dan dashboard
    url(r'^$', index, name='index'),
    url(r'^dashboard/$', dashboard, name='dashboard'),

    #movie
    url(r'^movie/list/$', movie_list, name='movie_list'),
    url(r'^movie/detail/(?P<id>.*)/$', movie_detail, name='movie_detail'),

    # Session dan Model (Watch Later)
    url(r'^movie/watch_later/add/(?P<id>.*)/$', add_watch_later, name='add_watch_later'),
    url(r'^movie/watch_later/$', list_watch_later, name='list_watch_later'),

    #API
    url(r'^api/movie/(?P<judul>.*)/(?P<tahun>.*)/$', api_search_movie, name='api_search_movie'),

    # Tambahan
    url(r'^movie/watch_later/delete/(?P<id>.*)/$', delete_watch_later, name='delete_watch_later'),
    url(r'^movie/watched_movie/add/(?P<id>.*)/$', add_watched_movies, name='add_watched_movies'),
    url(r'^movie/watched_movie/$', list_watched_movies, name='list_watched_movies'),
    url(r'^movie/watched_movie/delete/(?P<id>.*)/$', delete_watched_movies, name='delete_watched_movies'),

]
