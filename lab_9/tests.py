from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from http.cookies import SimpleCookie

# Create your tests here.

class Lab6UnitTest(TestCase):

	def test_lab_9_is_exist(self):
		response = Client().get('/lab-9/')
		self.assertEqual(response.status_code,200)

	def test_using_index_func(self):
		found = resolve('/lab-9/')
		self.assertEqual(found.func, index)

	def test_login_session_lab_9(self):
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session.save()
		response = self.client.get('/lab-9/')
		self.assertEqual(response.status_code,302)

	def test_profile_lab_9_not_login(self):
		response = self.client.get('/lab-9/profile/')
		self.assertEqual(response.status_code, 302)

	def test_profile_lab_9_already_login_some_perintilan_not_set(self):
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['access_token'] = 'something'
		session['kode_identitas'] = 'some code'
		session['role'] = 'mahasiswa'
		session.save()
		response = self.client.get('/lab-9/profile/')
		self.assertEqual(response.status_code, 200)

	def test_profile_lab_9_already_login_some_perintilan_set(self):
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['access_token'] = 'something'
		session['kode_identitas'] = 'some code'
		session['role'] = 'mahasiswa'
		session['drones'] = []
		session['opticals'] = []
		session['soundcards'] = []
		session.save()
		response = self.client.get('/lab-9/profile/')
		self.assertEqual(response.status_code, 200)

	def test_add_session_drones_not_set(self):
		response = self.client.get('/lab-9/add_session_drones/1/')
		self.assertEqual(response.status_code, 302)

	def test_add_session_soundcards_not_set(self):
		response = self.client.get('/lab-9/add_session_soundcards/1/')
		self.assertEqual(response.status_code, 302)

	def test_add_session_opticals_not_set(self):
		response = self.client.get('/lab-9/add_session_opticals/1/')
		self.assertEqual(response.status_code, 302)

	def test_add_session_drones_set(self):
		session = self.client.session
		session['drones'] = ['1','2','3','4','5']
		session.save()
		response = self.client.get('/lab-9/add_session_drones/6/')
		self.assertEqual(response.status_code, 302)

	def test_add_session_soundcards_set(self):
		session = self.client.session
		session['soundcards'] = ['1','2','3','4','5']
		session.save()
		response = self.client.get('/lab-9/add_session_soundcards/6/')
		self.assertEqual(response.status_code, 302)

	def test_add_session_opticals_set(self):
		session = self.client.session
		session['opticals'] = ['1','2','3','4','5']
		session.save()
		response = self.client.get('/lab-9/add_session_opticals/6/')
		self.assertEqual(response.status_code, 302)

	def test_delete_session_drones_set(self):
		session = self.client.session
		session['drones'] = ['1','2','3','4','5']
		session.save()
		response = self.client.get('/lab-9/del_session_drones/1/')
		self.assertEqual(response.status_code, 302)

	def test_delete_session_soundcards_set(self):
		session = self.client.session
		session['soundcards'] = ['1','2','3','4','5']
		session.save()
		response = self.client.get('/lab-9/del_session_soundcards/1/')
		self.assertEqual(response.status_code, 302)

	def test_delete_session_opticals_set(self):
		session = self.client.session
		session['opticals'] = ['1','2','3','4','5']
		session.save()
		response = self.client.get('/lab-9/del_session_opticals/1/')
		self.assertEqual(response.status_code, 302)

	def test_clear_session_drones_not_set(self):
		session = self.client.session
		session['drones'] = ['1','2','3','4','5']
		session.save()
		response = self.client.get('/lab-9/clear_session_drones/')
		self.assertEqual(response.status_code, 302)

	def test_clear_session_soundcards_not_set(self):
		session = self.client.session
		session['soundcards'] = ['1','2','3','4','5']
		session.save()
		response = self.client.get('/lab-9/clear_session_soundcards/')
		self.assertEqual(response.status_code, 302)

	def test_clear_session_opticals_not_set(self):
		session = self.client.session
		session['opticals'] = ['1','2','3','4','5']
		session.save()
		response = self.client.get('/lab-9/clear_session_opticals/')
		self.assertEqual(response.status_code, 302)

	def test_cookie_login(self):
		response = self.client.get('/lab-9/cookie/login/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'lab_9/cookie/login.html')
		self.client.cookies = SimpleCookie({'user_login': 'ayam.betutu', 'user_password': 'ayambetutu',})
		response = self.client.get('/lab-9/cookie/login/')
		html_response = response.content.decode('utf8')

	def test_cookie_auth_login(self):
		response = Client().post('/lab-9/cookie/auth_login/', {'username' : 'Anonymous', 'password' : 'A'})
		self.assertEqual(response.status_code, 302)
		response = Client().post('/lab-9/cookie/auth_login/', {'username' : 'ayam.betutu', 'password' : 'ayambetutu'})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-9/cookie/auth_login/')
		self.assertEqual(response.status_code, 302)

	def test_cookie_profile(self):
		response = self.client.get('/lab-9/cookie/profile/')
		self.assertEqual(response.status_code, 302)
		self.client.cookies = SimpleCookie({'user_login': 'sasas', 'user_password': 'sasas',})
		response = self.client.get('/lab-9/cookie/profile/')
		self.client.cookies = SimpleCookie({'user_login': 'ayam.betutu', 'user_password': 'ayambetutu',})
		response = self.client.get('/lab-9/cookie/profile/')

	def test_cookie_clear(self):
		self.client.cookies = SimpleCookie({'user_login': 'ayam.betutu', 'lang': 'ayambetutu',})
		response = self.client.get('/lab-9/cookie/clear/')




