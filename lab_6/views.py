from django.shortcuts import render

def index(request):
    response = {'author' : "Gibran Gifari Soesman"}
    return render(request, 'lab_6.html', response)
# Create your views here.
