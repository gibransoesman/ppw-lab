from django.contrib import admin

# Register your models here.
from .models import Friend, Mahasiswa
admin.site.register(Friend)
admin.site.register(Mahasiswa)